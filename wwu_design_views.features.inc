<?php
/**
 * @file
 * wwu_design_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_design_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
